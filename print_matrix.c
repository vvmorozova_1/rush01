#include <stdio.h>
//служебные столбцы и строки красные
int	**print_matrix(int **matrix, int size) //size как в задании (в нашем случае 4)
{
	for (int i = 0; i <= size + 1; i++)
		for(int j = 0; j <= size + 1; j++)
		{
			if (i == 0 || j == 0 || i == size + 1 || j == size + 1)
			{
				printf("\033[0;31m"); //Set the text to the color red
        			printf(" %d", matrix[i][j]);
        			printf("\033[0m"); //Resets the text to default color
			}
			else
				printf(" %d", matrix[i][j]);
		if (j == size + 1)
			printf("\n");
		}
}	
